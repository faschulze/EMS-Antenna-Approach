# R E A D M E 
===============

Implementing variability using Antenna preprocessor in the project [„Employee Payroll Management System“](https://www.dropbox.com/s/rmiuenk9oeqo3pw/Employee%20Payroll%20System.zip?dl=0) (originally by user Hyrex)


## Information on the open source project

The source code on this project can be found on [Gitlab](https://gitlab.com/yunocare/EMS-Antenna-Approach).

The project "EMS" is based on an open source project named *"Employee Payroll Management System"* 
published by user *"Hyrex"* (refined the project's state from: 2016-09-30). Source code on this
project can be found [here](https://www.dropbox.com/s/rmiuenk9oeqo3pw/Employee%20Payroll%20System.zip?dl=0),
as well as on Hyrex's [channel](https://www.youtube.com/channel/UC12Z6-QyYjcGmxgaLIxmFwg)

The project itself will be advanced to a Employee Management System with additionally builds in
functionalities for Administration and Report Generation. This project is created for academic 
purpose in the context software product line development using FeatureIDE plugin in Eclipse IDE.
See FeatureIDE's [official Documentation](https://featureide.github.io/) for further information.


## Installation

The project is created as a FeatureIDE project, please visit [GitHub](https://featureide.github.io/) and download the latest FeatureIDE version based on your system prerequisites. Since the approach is realized using the Antenna preprocessor for JAVA (check it out on [sourceforge](https://sourceforge.net/projects/antenna/)), it is important to set the project's composer to "Antenna" before composing and running the code. The project requires at least JAVA 6 to be run.

```sh
$ clone https://gitlab.com/yunocare/EMS-Antenna-Approach.git
$ //compose
$ //run main method in login.java
$ 
```

Note can specify your individual configuration in modifying the [default.xml](https://gitlab.com/yunocare/EMS-Antenna-Approach/blob/master/configs/default.xml) or creating a new configuration file and set it as the current one. (right-click + "set as current configuration")



## Domain Analysis

The variability implementation is based on an [feature model](https://gitlab.com/yunocare/EMS-Antenna-Approach/blob/master/model.xml).

![feature model](./documentation/model.png)


## Antenna Preprocessor - Implemented Features:

In the following a short overview will be given on which features are currently implemented or are in the stage of implementation.

| Feature | Description |
| ------ | ------ |
| Menu | The Menu parameter can be set to either „button menu“ or „list menu“. Dependent of the selection, the Main Menu will look differently. |
| Authentication | There are two ways of authentication implemented. On the one hand side there is a two factor authentication which is the combination of username and password. On the other hand side there is a three factor authentication that will be extended using a specific userrole. So the combination of username, password and role should match a database query. |
| Theme | A theme can be customized. Setting a specific theme has impact on font color, font size, as well as on the background image. There are three different custom default and one default theme, if no argument is set. |
| Localization | A localization runtime argument can be set. Dependent on this parameter the langugae will change, if no argument is set, the software profuct will be delivered in english language. Furthermore the localization argument has impact on the date and timestamps as well. In english the date pattern is YYYY-MM-DD, whereas in german language the date is displayed as DD.MM.YYY. |
| Logging | A log file will be generated to log all activities per session, containing for example all login-approaches, all changes and all user interactions. To store all log files, a seperated directory „logs“ will be created (if not existing) and each log file name is based on a timestamp, to uniquely identify each session. This can make sense in the business case of delivering a cost-free test version and delivering an overall commercial version. |
| Audit Trail | Additional to logging opportunities, the project line provides an audit trail, in that all changes, sessions and inserts are documented. This functionality is enabled by default. Disabling this functionality is currently in doing. This can make sense in the business case of delivering a cost-free test version and delivering an overall commercial version. |

	
## Implementing variability using the Antenna preprocessor

Unfortunately preprocessor are no nativ part of the JAVA programming language, therefore the Antenna project provides a set of Ant tasks that can be used to compile preprocessor annotations. The approach is pretty simple, a feature's code can be annotated in the code using *#if*. *#ifdef* or *#ifndef* followed by the feature's name. Preprocessor annotations are a good approach when it comes to very granular variability implementation, means that proprocessor annotations can even annotate a single character. Feature traceability can be challenging due to scattered and tangled code, therefore you can use the CIDE tool support. See [CIDE on github](http://ckaestne.github.io/CIDE/) for more information.

## Project Structure

The project consists of following parts:

 - de.ovgu.isp.ems.db (contains all database related classes)
 - de.ovgu.isp.ems.core (contains core functionalities)
 - de.ovgu.isp.ems.core.logger (contains logging classes and configurations)
 - de.ovgu.isp.ems.ui (contains optional UI components) 
 - de.ovgu.isp.ems.ui.core (contains all core UI components)

**In this version there is currently no other way than using the packed SQLite database that can be found in "src/empnet.sqlite".** *(default login is "admin"/"pass")*
A Different database can be connected easily using the related JDBC connection instead of the current SQLite database connection in de.ovgu.isp.ems.db.Database.java.


## Acadamic Purpose

The project is created for academic purpose in subject of software product line implementation techniques. 



## Status

last updated: 2018-03-16


## Short Insights – FeatureIDE Outline

To give a short insight into Antenna Implementation from the FeatureIDE Outline point of view:

Implementation in class login.java:

![login.java](https://i.imgur.com/cihuuzh.png)

Implementation in class MainMenu.java:

![MainMenu.java](https://i.imgur.com/NRWmgFx.png)
