/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee.payroll.system;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 *
 * @author Hyrex
 * 
 * Login data for Admin is:
 * User "Admin" - Password "pass"
 */
import java.awt.event.KeyEvent;
import java.io.File;
import java.awt.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.*;

import org.apache.logging.log4j.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;

public class login extends javax.swing.JFrame {
	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement pst = null;

	/**
	 * Creates new form Login_jframe
	 */
	public login(String s) {

	}

	public login() {
		initComponents();
		conn = db.java_db();
		Toolkit toolkit = getToolkit();
		Dimension size = toolkit.getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);
		currentDate();

	}

	/**
	 * This method prints current Time + Date
	 */
	public void currentDate() {

		Timer t = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Assumes clock is a custom component
				switch (currentLocale.getCountry()) {
				// TODO: set local time format for selected localization
				case "US":
					lbl_time.setText(new Date().toString());
				case "DE":
					lbl_time.setText(new Date().toString());
				default:
					lbl_time.setText(new Date().toString());
				}
			}

		});
		t.start();
		Calendar cal = new GregorianCalendar();
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		// set local date format:
		if (currentLocale.getCountry().equals("DE")) {
			// German
			txt_date.setText(" | " + day + "." + (month + 1) + "." + year + " | ");
		}
		if (currentLocale.getCountry().equals("RU")) {
			// TODO: research: does a specific date format exists?
			txt_date.setText(" | " + (month + 1) + "-" + day + "-" + year + " | ");
		} else {
			// English
			txt_date.setText(" | " + (month + 1) + "/" + day + "/" + year + " | ");
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	// <editor-fold defaultstate="collapsed" desc="Generated
	// Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jMenuItem1 = new javax.swing.JMenuItem();
		jMenu1 = new javax.swing.JMenu();
		jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
		jPopupMenu1 = new javax.swing.JPopupMenu();
		jPanel2 = new javax.swing.JPanel();
		cmd_Login = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		txt_username = new javax.swing.JTextField();
		txt_password = new javax.swing.JPasswordField();
		txt_combo = new javax.swing.JComboBox();
		jLabel3 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jMenuBar1 = new javax.swing.JMenuBar();
		jMenu2 = new javax.swing.JMenu();
		jMenuItem2 = new javax.swing.JMenuItem();
		jMenu3 = new javax.swing.JMenu();
		txt_date = new javax.swing.JMenu();
		lbl_time = new javax.swing.JMenu();

		jMenuItem1.setText("jMenuItem1");

		jMenu1.setText("jMenu1");

		jRadioButtonMenuItem1.setSelected(true);
		jRadioButtonMenuItem1.setText("jRadioButtonMenuItem1");

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(false);

		jPanel2.setLayout(null);

		cmd_Login.setText(messages.getString("login"));
		cmd_Login.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cmd_LoginActionPerformed(evt);
			}
		});
		cmd_Login.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				cmd_LoginKeyPressed(evt);
			}
		});
		jPanel2.add(cmd_Login);
		cmd_Login.setBounds(160, 470, 70, 30);

		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText(messages.getString("user") + ":");
		jPanel2.add(jLabel1);
		jLabel1.setBounds(10, 360, 70, 14);

		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText(messages.getString("pwd") + ":");
		jPanel2.add(jLabel2);
		jLabel2.setBounds(10, 400, 70, 14);

		jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
		jLabel6.setForeground(new java.awt.Color(255, 255, 255));
		jLabel6.setText(messages.getString("enterlogindata"));
		jPanel2.add(jLabel6);
		jLabel6.setBounds(10, 320, 390, 14); // previous: (10, 320, 241, 14)
		jPanel2.add(txt_username);
		txt_username.setBounds(100, 350, 132, 30);

		txt_password.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				txt_passwordActionPerformed(evt);
			}
		});
		txt_password.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				txt_passwordKeyPressed(evt);
			}
		});
		jPanel2.add(txt_password);
		txt_password.setBounds(100, 390, 132, 30);

		// #if ThreeFA
		// @ txt_combo.setModel(new javax.swing.DefaultComboBoxModel(
		// @ new String[] { "Admin", "Sales", "Undefined1", "Undefined2" }));
		// @ txt_combo.addActionListener(new java.awt.event.ActionListener() {
		// @ public void actionPerformed(java.awt.event.ActionEvent evt) {
		// @ txt_comboActionPerformed(evt);
		// @ }
		// @ });
		// @ jPanel2.add(txt_combo);
		// @ txt_combo.setBounds(100, 430, 130, 30);
		// @ jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		// @ jLabel3.setText(messages.getString("selectrole") + ":");
		// @ jPanel2.add(jLabel3);
		// @ jLabel3.setBounds(10, 440, 100, 14);
		// #endif

		jLabel4.setForeground(new java.awt.Color(255, 255, 255));

		// TODO: customize backgrounds
		/**
		 * Set theme related background picture
		 * 
		 * bk.jpg: bacl white
		 * 
		 * bk2.jpg: statistics background
		 * 
		 * bk3.jpg: hyrex computer
		 * 
		 * bk4.jpg: wood optic
		 */
		// #if Theme1
		// @ jLabel4.setIcon(new javax.swing.ImageIcon(getClass().
		//@ getResource("/employee/payroll/system/Images/bk3.jpg")));
//@		// // NOI18N
		// #elif Theme2
//@		jLabel4.setIcon( new javax.swing.ImageIcon(getClass().
//@		 getResource("/employee/payroll/system/Images/bk4.jpg")));
//@		// // NOI18N
		// #elif Theme3
		// @ jLabel4.setIcon(new javax.swing.ImageIcon(getClass().
		// @ getResource("/employee/payroll/system/Images/bk2.jpg")));
//@		// // NOI18N
		// #else
		 jLabel4.setIcon(new javax.swing.ImageIcon(getClass().
		 getResource("/employee/payroll/system/Images/bk.jpg")));
		// // NOI18N
		// #endif

		jPanel2.add(jLabel4);
		jLabel4.setBounds(0, 0, 790, 510);

		jMenu2.setText(messages.getString("file"));

		jMenuItem2.setAccelerator(
				javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
		jMenuItem2
				.setIcon(new javax.swing.ImageIcon(getClass().getResource("/employee/payroll/system/Images/Exit.png"))); // NOI18N
		jMenuItem2.setText(messages.getString("exit"));
		jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jMenuItem2ActionPerformed(evt);
			}
		});
		jMenu2.add(jMenuItem2);

		jMenuBar1.add(jMenu2);
		jMenuBar1.add(jMenu3);

		txt_date.setText(messages.getString("date"));
		jMenuBar1.add(txt_date);

		lbl_time.setText(messages.getString("time"));
		jMenuBar1.add(lbl_time);

		setJMenuBar(jMenuBar1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addComponent(jPanel2,
								javax.swing.GroupLayout.PREFERRED_SIZE, 788, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(0, 0, Short.MAX_VALUE)));
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup().addComponent(jPanel2,
								javax.swing.GroupLayout.PREFERRED_SIZE, 509, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(0, 0, Short.MAX_VALUE)));

		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void txt_passwordActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_txt_passwordActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_txt_passwordActionPerformed

	private void cmd_LoginActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmd_LoginActionPerformed
		// TODO add your handling code here:

		// check if username field is empty
		if (txt_username.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_username_field"));
		}
		// check if password field is empty
		else if (txt_password.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_password_field"));
		} else {
			// Prepare SQL statement to check if credentials are valid
			String sql = "select id,username,password,division from Users Where (username =? and password =?)";

			// #if ThreeFA
			// @ sql = "select id,username,password,division from Users Where (username =?
			// @ and password =? and division =?)";
			// #else
			 sql = "select id,username,password,division from Users Where (username =?"+
			 "and password =?)";
			// #endif

			try {
				int count = 0; // Login-Status

				// Prepare SQL statement: SELECT * FROM USERS Bedingung input values
				pst = conn.prepareStatement(sql);
				pst.setString(1, txt_username.getText());
				pst.setString(2, txt_password.getText());
				// #if ThreeFA
				// @ pst.setString(3, txt_combo.getSelectedItem().toString()); // only if 3FA
				// #endif
				// Execute Query
				rs = pst.executeQuery();
				while (rs.next()) {
					int id = rs.getInt(1);
					Emp.empId = id;
					count = count + 1;
				}

				String access = "";
				// #if ThreeFA
				// @ access = (txt_combo.getSelectedItem().toString());
				// #else
				 access = "Admin";
				// #endif

				if (access == "Admin") {

					if (count == 1) {
						// JOptionPane.showMessageDialog(null, messages.getString("success"));
						MainMenu j = new MainMenu();
						j.setVisible(true);
						this.dispose();
						// #if AuditTrail
						// Create new Date object to store session date in TABLE AUDITS
						 Date currentDate = GregorianCalendar.getInstance().getTime();
						 DateFormat df = DateFormat.getDateInstance();
						 String dateString = df.format(currentDate);
						// Create new Date object to store session time stamp in TABLE AUDITS
						 Date d = new Date();
						 SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
						 String timeString = sdf.format(d);
						// insert session into database
						 int value = Emp.empId;
						 String reg = "insert into Audit (emp_id,date,status) values ('" + value +
						 "','" + timeString
						 + " / " + dateString + "','Logged in')";
						 pst = conn.prepareStatement(reg);
						 pst.execute();
						 this.dispose();
						// #endif
					}

					else if (count > 1) {
						JOptionPane.showMessageDialog(null, messages.getString("duplicate_login"));
					} else {
						JOptionPane.showMessageDialog(null, messages.getString("incorrect_password"));
					}
				}
				// if user role is Sales:
				else {
					// (access == "Sales")

					if (count == 1) {
						// JOptionPane.showMessageDialog(null, messages.getString("sucess"));
						System.out.println("---");
						MainMenu obj = new MainMenu();
						System.out.println("---");
						obj.setVisible(true);
						// #if AuditTrail
						 Date currentDate = GregorianCalendar.getInstance().getTime();
						 DateFormat df = DateFormat.getDateInstance();
						 String dateString = df.format(currentDate);

						 Date d = new Date();
						 SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
						 String timeString = sdf.format(d);

						 String value0 = timeString;
						 String values = dateString;

						 int value = Emp.empId;
						 String reg = "insert into Audit (emp_id,date,status) values ('" + value +
						 "','" + value0 + " / "
						 + values + "','Logged in')";
						 pst = conn.prepareStatement(reg);
						 pst.execute();
						// #endif
						this.dispose();
					} else {
						JOptionPane.showMessageDialog(null, messages.getString("incorrect_password"));
					}
				}
			} catch (SQLException e)

			{
				JOptionPane.showMessageDialog(null, e);
				e.printStackTrace();

			} finally {

				try {
					rs.close();
					pst.close();

				} catch (Exception e) {

				}
			}

		}

	}// GEN-LAST:event_cmd_LoginActionPerformed

	/**
	 * Exit
	 * 
	 * @param evt
	 */
	private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuItem2ActionPerformed
		// TODO add your handling code here:
		System.exit(0);
	}// GEN-LAST:event_jMenuItem2ActionPerformed

	private void cmd_LoginKeyPressed(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_cmd_LoginKeyPressed
		// TODO add your handling code here:
		txt_passwordKeyPressed(evt);
	}// GEN-LAST:event_cmd_LoginKeyPressed

	private void txt_passwordKeyPressed(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txt_passwordKeyPressed
		// TODO add your handling code here:

		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			// TODO: call cmd_LoginActionPerformed(evt);

			String sql = "select id,username,password,division from Users Where (username =? and password =?)";
			// #if ThreeFA
			// @ sql = "select id,username,password,division from Users Where (username =?
			// @ and password =? and division =?)";
			// #if ApplicationLogging
			// @ ApplicationLogger.logger.info("Trying to perform login with following
			// @ credentials: username="+ txt_username.getText() + " and password=" +
			// @ txt_password.getText() + " role="+ txt_combo.getSelectedItem().toString());
			// #endif
			// @ sql = "select id,username,password,division from Users Where (username =?
			// @ and password =?)";
			// #if ApplicationLogging
			// @ ApplicationLogger.logger.info("Trying to perform login with following
			// @ credentials: username="+ txt_username.getText() + " and password=" +
			// @ txt_password.getText());
			// #endif
			// #endif

			try {
				int count = 0;

				pst = conn.prepareStatement(sql);

				pst.setString(1, txt_username.getText());
				pst.setString(2, txt_password.getText());
				// #if ThreeFA
				// @ pst.setString(3, txt_combo.getSelectedItem().toString());
				// #endif
				rs = pst.executeQuery();

				{
				}
				while (rs.next()) {
					int id = rs.getInt(1);
					Emp.empId = id;
					count = count + 1;
				}
				String access = "";
				// #if ThreeFA
				// @ access = (txt_combo.getSelectedItem().toString());
				// #else
				 access = "Admin";
				// #endif
				if (access == "Admin") {

					if (count == 1) {
						JOptionPane.showMessageDialog(null, messages.getString("sucess"));
						// #if ApplicationLogging
//@						 ApplicationLogger.logger.info("Login erfolgreich");
						// #endif
						MainMenu j = new MainMenu();
						j.setVisible(true);
						this.dispose();
						// #if AuditTrail
						 Date currentDate = GregorianCalendar.getInstance().getTime();
						 DateFormat df = DateFormat.getDateInstance();
						 String dateString = df.format(currentDate);
						 Date d = new Date();
						 SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
						 String timeString = sdf.format(d);
						 String value0 = timeString;
						 String values = dateString;
						 int value = Emp.empId;
						 String reg = "insert into Audit (emp_id,date,status) values ('" + value +
						 "','" + value0 + " / "
						 + values + "','Logged in')";
						 pst = conn.prepareStatement(reg);
						 pst.execute();
						 this.dispose();
						// #endif

					}

					else if (count > 1) {
						JOptionPane.showMessageDialog(null, messages.getString("duplicate_login"));
						// #if ApplicationLogging
//@						 ApplicationLogger.logger.error("Login failed due to duplicated login.");
						// #endif
					}

					else {
						JOptionPane.showMessageDialog(null, messages.getString("incorrect_password"));
						// #if ApplicationLogging
//@						 ApplicationLogger.logger.error("Login failed due to incorrect password.");
						// #endif

					}

				} else if (access == "Sales") {

					if (count == 1) {
						MainMenu j = new MainMenu();
						j.setVisible(true);

						// #if AuditTrail
						 Date currentDate = GregorianCalendar.getInstance().getTime();
						 DateFormat df = DateFormat.getDateInstance();
						 String dateString = df.format(currentDate);
						 Date d = new Date();
						 SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
						 String timeString = sdf.format(d);
						 String value0 = timeString;
						 String values = dateString;
						 int value = Emp.empId;
						 String reg = "insert into Audit (emp_id,date,status) values ('" + value +
						 "','" + value0 + " / " + values + "','Logged in')";
						 pst = conn.prepareStatement(reg);
						 pst.execute();
						this.dispose();
						// Logging:
						// #if ApplicationLogging
						// @ApplicationLogger.logger.error("Login successful.");
						// #endif
						// #endif

					}

					else {
						JOptionPane.showMessageDialog(null, messages.getString("incorrect_password"));
						// Logging:
						// #if ApplicationLogging
//@						 ApplicationLogger.logger.error("Login failed due to incorrect password.");
						// #endif
					}

				}

			} catch (Exception e)

			{
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e);

			} finally {

				try {
					rs.close();
					pst.close();

				} catch (Exception e) {
					e.printStackTrace();

				}
			}

		}

	}// GEN-LAST:event_txt_passwordKeyPressed

	public ResourceBundle getBundle() {
		messages = ResourceBundle.getBundle("MessagesBundle", currentLocale);
		return messages;
	}

	private void txt_comboActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_txt_comboActionPerformed
		// TODO add your handling code here:
	}// GEN-LAST:event_txt_comboActionPerformed

	private void setLanguagePreferences(String language) {

		// #if DE
		 System.out.println("Set language to german.");
		 currentLocale = new Locale("de", "DE");
		// #elif FR
		// @ System.out.println("Set language to french");
		// @ currentLocale = new Locale("fr", "FR");
		// #elif RU
		// @ System.out.println("Set language to russian");
		// @ currentLocale = new Locale("ru", "RU");
		// #else
//@		 System.out.println("Set language to english");
//@		 currentLocale = new Locale("en", "US");
		// #endif
		Locale.setDefault(currentLocale);

	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		// Set language preferences:
		String s = "";
		// #if DE
		 s="de";
		// #elif RU
		// @ s="ru";
		// #elif FR
		// @ s="fr";
		// #else
//@		 s="en";
		// #endif
		login l = new login(null);
		l.setLanguagePreferences(s);

		/* Set the Nimbus look and feel */
		// <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
		// (optional) ">
		/*
		 * If Nimbus (introduced in Java SE 6) is not available, stay with the default
		 * look and feel. For details see
		 * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| javax.swing.UnsupportedLookAndFeelException ex) {
			ex.printStackTrace();
			java.util.logging.Logger.getLogger(login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new login().setVisible(true);
			}
		});
	}

	// default values:
	public static Locale currentLocale = new Locale("en", "US");
	public ResourceBundle messages = getBundle();

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton cmd_Login;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JMenu jMenu1;
	private javax.swing.JMenu jMenu2;
	private javax.swing.JMenu jMenu3;
	private javax.swing.JMenuBar jMenuBar1;
	private javax.swing.JMenuItem jMenuItem1;
	private javax.swing.JMenuItem jMenuItem2;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPopupMenu jPopupMenu1;
	private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
	private javax.swing.JMenu lbl_time;
	private javax.swing.JComboBox txt_combo;
	private javax.swing.JMenu txt_date;
	private javax.swing.JPasswordField txt_password;
	private javax.swing.JTextField txt_username;
	// End of variables declaration//GEN-END:variables
}
